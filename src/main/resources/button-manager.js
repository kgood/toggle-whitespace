
labelText = ((document.URL.search(/(\?|&)w=1/) != -1) ? "Don't i" : "I") + "gnore whitespace";

tooltipText = labelText + " in the diff for this changeset";

newUrl = function() {
	var oldUrl = document.URL;

	//if whitespace is already being ignored, toggle it off
	if (oldUrl.search(/(\?|&)w=1/) != -1) {
		return oldUrl.replace(/(\?|&)w=1&?/, "$1")
		              //cleanup 
		              .replace(/\?(#.*|$)/, "$1");
	} 

	// add argument w=1 to url, which means ignore whitespace to stash
  if (oldUrl.indexOf('?') != -1) {
    return oldUrl.replace('?', "?w=1&");
  } 
  else {
    return oldUrl.replace(/(#.*|$)/, "?w=1$1");
  }
}